package com.example.DCloset.model.member;

import com.example.DCloset.enums.MemberGrade;
import com.example.DCloset.enums.PayDay;
import com.example.DCloset.enums.PayWay;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Membership {
    // 멤버십 가입 신청 시 멤버 등급, 결제수단에 대한 정보 변경

    private MemberGrade memberGrade;
    private PayWay payWay;
    private String payInfo;
    private PayDay payDay;
}
