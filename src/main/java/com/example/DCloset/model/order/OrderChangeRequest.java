package com.example.DCloset.model.order;


import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.MemberGrade;
import com.example.DCloset.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderChangeRequest {

    private MemberGrade memberGrade;
    private LocalDate orderDate;
    private LocalDate desiredDate;
    private LocalDate deadlineDate;
    private OrderStatus orderStatus;
}
