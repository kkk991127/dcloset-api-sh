package com.example.DCloset.model.questionBulletin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


public class QuestionBulletInChangeMemberRequest {


    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;
}
