package com.example.DCloset.model.delivery;

import com.example.DCloset.enums.DeliveryType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class DeliveryChangeTypeRequest {

    private DeliveryType deliveryType;

}
