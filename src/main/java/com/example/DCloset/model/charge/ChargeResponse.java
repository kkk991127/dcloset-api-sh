package com.example.DCloset.model.charge;

import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.PayType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class ChargeResponse {


    private Long id;
    private Member member;
    private LocalDate actualPayDay;
    private PayType payType;
    private Integer finalAmount;



}
